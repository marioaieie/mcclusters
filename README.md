# README #

MCclusters is a Bayesian python code for galaxy clusters modelling. It uses a multi-wavelength approach to break degeneracies between parameters, in particular to reconstruct the true 3D shape. It follows the ideas and algorithm presented in [Morandi et al. 2012](https://ui.adsabs.harvard.edu/#abs/2012MNRAS.425.2069M/abstract) and [Limousin et al. 2013](https://ui.adsabs.harvard.edu/#abs/2013SSRv..177..155L/abstract).

Its modular structure allows for easy expansions of the physical models and the use of arbitrary priors.


### How do I get set up? ###

To install it, just clone this repository:

```
git clone https://bitbucket.org/marioaieie/mcclusters.git
```

### How do I use this? ###
In the [Examples](https://bitbucket.org/marioaieie/mcclusters/raw/master/Examples/) folder you can find two jupyter notebooks that show how to MCclusters.

The first, generate\_mocks\_simple (click [here](https://nbviewer.jupyter.org/urls/bitbucket.org/marioaieie/mcclusters/raw/master/Examples/generate_mocks_simple.ipynb) to open it in nbviewer), can be used to create mock observations.

The second, mcmc\_fit (click [here](https://nbviewer.jupyter.org/urls/bitbucket.org/marioaieie/mcclusters/raw/master/Examples/mcmc_fit.ipynb) to open it in nbviewer), take the files created in the previous notebook and then runs the mcmc sampler.

The generate\_mocks\_simple notebook also shows the format for the input files used by the mcclusters.utils.read_data function.

### Who do I talk to? ###
This code has been developed at [Laboratoire d'Astrophysique de Marseille](http://lam.fr/) by [Mario Bonamigo](www.nbi.dk/~bonamigo/).

WARNING! This is work in progress! If you are interested in the ideas or code you find here, please get in touch! We welcome feedback via the issues page.

All content Copyright 2015 the authors. The code is distributed under the MIT license.
