import os.path
import copy
import time
import pickle
import numpy as np
import scipy.stats

import astropy.io.fits as fits
from astropy.table import Table
from astropy.cosmology import FlatLambdaCDM
from astropy import units
from astropy import constants as cn
import emcee

import mcclusters
from mcclusters.models.sutolab import loglike as model
from mcclusters import initial_values


components = 'SBT'
iters = 100
nwalkers = 96
threads = 1
doSample = False
savechain = False
optimize = False
trace = False

# path with fits files
path = ''
# chain output file
dbname = 'chain_'+components+'0.pickle'


# Set the cosmology
cosmo = FlatLambdaCDM(H0=70.,Om0=0.3)
redsh = 0.3
rhoc = cosmo.critical_density(redsh)


# Number of pixels in the model
Nx = 96
Ny = 96
Nz = 13


#tpar = copy.deepcopy(model.tpar)
tpar = initial_values.test()

for ind in tpar.keys():
	tpar[ind]['Observed'] = False
# To fix some parameters use something like:
# tpar['rs']['Observed'] = True
# tpar['rs']['Value'] = 300.
# and if you want priors


if savechain:
    # Check if dbname exist
    if os.path.isfile(dbname):
        raise Exception('File '+dbname+' already exist! I will stop here.')
    # and open it, if possible
    outfile = open(dbname,'wb')


###	Open strong lensing fits file and load the data into sl_obs
sl_obs = {}
infile = path+'sl_mock.fits' 
hdulist1 = fits.open(infile)
for hdu in hdulist1:
    sl_obs[hdu.name.lower()] = hdu.data


###	Open brightness map fits file and load the data into bmap_obs
bmap_obs = {}
infile = path+'bmap_mock.fits' 
hdulist1 = fits.open(infile)
for hdu in hdulist1:
    bmap_obs[hdu.name.lower()] = hdu.data

bmap_obs['scalebmap'] = hdulist1[0].header['scale']
bmap_obs['bkg'] = hdulist1[0].header['bkg']


###	Open X-ray temperature fits file and load the data into bmap_obs
Xtemp_obs = Table.read(path+'xtemp_mock.fits')

# In case we need different masks
#xsl, ysl = np.meshgrid(sl_obs['x'], sl_obs['y'], sparse=False, indexing='xy')
#rrSL = np.sqrt(xsl**2+ysl**2)
#sl_obs['mask'] = np.ones(sl_obs['avg'].shape)
#sl_obs['mask'][(rrSL < 45.) | (rrSL > 400.)] = 0

#xbmap, ybmap = np.meshgrid(bmap_obs['xbmap'], bmap_obs['ybmap'], sparse=False, indexing='xy')
#rrbmap = np.sqrt(xbmap**2+ybmap**2)
#bmap_obs['maskbmap'][(rrbmap < 100.) | (rrbmap > 700.)] = 0


# Make the grid for the model
rref_out = bmap_obs['ybmap'][-1] + (bmap_obs['ybmap'][-1] - bmap_obs['ybmap'][-2])/2
side = 2 * rref_out * units.kpc
x = (np.linspace(-1,1.,Nx,endpoint=False)+1./Nx)*side/2.
y = (np.linspace(-1,1.,Ny,endpoint=False)+1./Ny)*side/2.
z = (np.linspace(0.,Nz,Nz,endpoint=False))*side/Nx*4.
dz = z[1]-z[0]
scale = side/Nx

xx, yy, zz = np.meshgrid(x, y, z, sparse=False, indexing='xy')
r = np.sqrt( xx**2 + yy**2 + zz**2 )
coord = np.array([xx,yy,zz,r])
pol_coord = np.sqrt( xx[:,:,0]**2 + yy[:,:,0]**2 ).value


# It's better to do this computations outside the MCMC sampler
slscale = rhoc.value * dz.to('cm').value
sl_obs['mask'] = (sl_obs['mask'] == 1.).flatten()
sl_obs['avg'] = sl_obs['avg'].flatten()[sl_obs['mask']]
sl_obs['fisher'] = sl_obs['fisher'][sl_obs['mask']]
#sl_obs['fisher'] = sl_obs['fisher'][sl_obs['mask']][:,sl_obs['mask']]

bmap_obs['scale'] = bmap_obs['k']*(bmap_obs['scalebmap']*units.kpc.to("cm"))**2 *(dz.to('cm').value)
bmap_obs['maskbmap'] = bmap_obs['maskbmap'] == 1.

pscale = (4*np.pi*rhoc*cn.G.cgs).value* units.kpc.to('cm')**2
tscale = cn.u.to('g').value* 0.6  *units.erg.to('keV')#*pscale



pos0 = []
pmin = []
pmax = []
j = 0
for p in tpar.items():
    if not p[1]['Observed']:
        print(p[0])
        pmin.append(p[1]['Lower'])
        pmax.append(p[1]['Upper'])
        pos0.append(p[1]['Value'])
        p[1]['Pos'] = j
        j += 1

pmin = np.array(pmin)
pmax = np.array(pmax)


args = (model,components,tpar,coord,pol_coord,sl_obs,bmap_obs,Xtemp_obs,slscale,pscale,tscale,1.,0.,trace)


if optimize:
    import scipy.optimize as op
    nll = lambda *args: -mcclusters.lnprob(*args)[0]
    opt = op.minimize(nll, pos0, method = 'nelder-mead', args=args)
    if (opt['status']): raise Exception(opt['message'])
    pos0 = opt['x']
    print(opt['message'])
    print('pos0:\t'+str(pos0))
    print('Chis:\t'+str(mcclusters.lnprob(pos0,*args)[1]))


if (doSample):
    # Create and initialise (p0) walkers
    ndim = len(pos0)
    #pos = pos0 + np.random.uniform( low=pmin, high=pmax, size=(nwalkers,ndim) )
    pos = pos0 + np.random.normal( scale=0.05, size=(nwalkers,ndim) )
    pos = np.minimum( pmax, np.maximum( pmin, pos ) )


    # Initialise EnsembleSampler
    sampler = emcee.EnsembleSampler(nwalkers, ndim, mcclusters.lnprob,
                                    args=args, threads=threads)


    elaps = time.time()
    i = 1
    for result in sampler.sample(pos, iterations=iters, storechain=True):
        print('',end='')
        print(str(i)+"\t%.2f"%(i/iters*100)+"%\t"+str(int((time.time()-elaps)))+"s",end='\r')
        i += 1


    print('\n')

    if savechain:
        output = {}
        output['chain'] = sampler.chain[:]
        output['blob'] = sampler.blobs[:]
        output['tpar'] = copy.deepcopy(tpar)
        for p in output['tpar'].values():
            try:
                p.pop('Prior')
            except:
                pass
        pickle.dump(output,outfile)
        outfile.close()

