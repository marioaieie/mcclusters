import os.path
import copy
import numpy as np
import scipy.stats
import scipy.interpolate
import matplotlib.pyplot as plt

import astropy.io.fits as fits
from astropy.table import Table
from astropy.cosmology import FlatLambdaCDM
from astropy import units
from astropy import constants as cn
import colormaps as cmaps


#from .models.triaxial_nfw import TriaxialNFW as Model
#from .models.spherical_nfw import SphericalNFW as Model

from . import utils

######################## Create the data ########################


# Set the cosmology
cosmo = FlatLambdaCDM(H0=70.,Om0=0.3)
redsh = 0.3

# Make the grid for the model
Nx = 64
Ny = 64
Nz = 128

side = (1024-1) * 0.492 * units.arcsec \
       * cosmo.kpc_proper_per_arcmin(redsh).to('kpc/arcsec')
side = 100 * units.arcsec * cosmo.kpc_proper_per_arcmin(redsh).to('kpc/arcsec')
side = 1000 * units.kpc

xx, yy, zz, rr, pol_coord = utils.create_grid( Nx, Ny, Nz, side )
coord = np.array([xx,yy,zz,rr])
x = xx[:,0,0]
y = yy[0,:,0]

dz = zz[0,0,1] - zz[0,0,0]
scale = side / ( Nx - 1 ) # Pixelsize
print( 'dz: ' + str( dz ) + '\tpx: ' + str( scale ) )


# Some scaling constants
rhoc = cosmo.critical_density(redsh)
C0 = 4 * np.pi * cn.G * rhoc * side.unit**2
tscale = cn.u * 0.61 / units.keV
slscale = rhoc * units.kpc

#model = Model( coord, tscale=( tscale * C0 ).cgs.value )



# And finally compute the model
def generate( model, tpar, components='SBTZ'):
    """
        from mcclusters.models import sutolab as model
        from mcclusters.models.sutolab import tpar

        dm2d, em2d, t2d, dtt2d = 
            mcclusters.generate_mock.generate(model, tpar, components='SBTZ')
    """

    # Let's get the parameters
#    tpar = initial_values.test()
    params = {}
    for par in tpar.items():
    	params[par[0]] = par[1]['Value']

    return model.loglike( components, params, coord, pscale, tscale )



def get_observables( model, sidesl=None, path='', redges=None ):

    
    dm2d = model.dm2d()
    em2d = model.em2d()
    t2d = model.t2d()
    dtt2d = model.dt2d()

    
    ######################## Strong lensing ########################
   
    if sidesl:

        sidesl = cosmo.kpc_proper_per_arcmin(redsh).to('kpc/arcsec')
        sidesl *= 200 * units.arcsec
        Nxsl = 50
        Nysl = 50

        slx = np.linspace( -1, 1., Nxsl ) * sidesl / 2.
        sly = np.linspace( -1, 1., Nysl ) * sidesl/ 2.

        spline = scipy.interpolate.RectBivariateSpline( xx[:,0,0],
                                                        yy[0,:,0],
                                                        np.log10( dm2d ) )
        dm2d = np.maximum(0., 10**spline( slx, sly ) )


    # Rescale the mass map to meaningful units and add some noise
    slscale =  rhoc * units.kpc
    dm2d = dm2d * slscale.cgs.value



    ######################## X-ray temperature ########################
    # Bin the 2d map in circular annuli
#    rref = np.linspace(0.**(2./3.),(side.value*0.9/2.)**(2./3.),8)**(3./2.)
    if redges is None:
        redges = np.logspace( np.log10(60.), np.log10( side.value * 0.9 / 2. ), 9 )
    rin = redges[:-1]
    rout = redges[1:]
    rref = redges[:-1] + np.diff(redges)/2.

    ii2 = np.digitize( pol_coord, bins=redges )
    ## We should chec
    weigth = np.maximum( 1e-20, np.bincount( ii2.flat, em2d.flat ) )
    tfit_x0 = ( np.bincount( ii2.flat, (t2d * em2d).flat ) / weigth )[1:-1]




    ######################## X-ray Surface Brightness ########################
    # Bin the 2d map in circular annuli

    rate = 60. * np.ones( em2d.shape ) / units.s
    livetime = 8e4 * units.s
    bkg = 0.1
    
    
    norm = rate * livetime * 1e-14 * 0.826 * units.cm**5  \
    / ( 4. * np.pi * cosmo.angular_diameter_distance( redsh )**2 * (1+redsh)**2 )
    
    em2d = ( norm * units.cm**-6 * units.kpc * scale**2 ).cgs * model.em2d()  + bkg



    return dm2d, em2d, tfit_x0


