import numpy as np
import scipy.integrate
import scipy.interpolate
import scipy.stats
from astropy import constants as cn
from astropy import units
import copy
import astropy.io.fits as fits
from astropy.table import Table
import pickle
import emcee


class stat(object):
   
    def __init__( self, model, observables, fitted, hyper=False ): 
        self.model = model
        self.observables = observables
        self.fitted = fitted
        self.hyper = hyper

    def __getnewargs__( self ):
        return self.model, self.observables, self.fitted

        
    def lnprior( self, p ):

#        print('computing priors' + str(p[0]))
        lp = 0
        for i,par in enumerate(self.fitted):
            lp += par.prior.logpdf( p[i] )
        return lp
        

    def lnprob( self, pos ):

#        print('updating values' + str(pos[0]))
#        self.model.s.itemset( p[0] )
#        self.model.A.itemset( p[1] )
        if self.hyper:
#            hyper = np.zeros( len( self.observables ) )
            hyper = pos[ -len( self.observables ): ]
#            hyper[-1] = 1 - np.sum( hyper[:-1]  )
            for obs, h in zip( self.observables, hyper ):
                obs.hyper = h
            pos = pos[ : -len( self.observables ) ]
            
        for v,p in zip( self.fitted, pos ):
            v.itemset( p )

        self.model.update()
        
#        try:
#            if self.model.s > self.model.q :
#                return -np.inf
#        except AttributeError:
#            pass

        return np.sum( [ obs.hyper * obs.loglike()  for obs in self.observables ] )
#        return np.sum( [ obs.hyper * obs.loglike() - np.log(obs.hyper) * obs.obs.size/2 - obs.hyper for obs in self.observables ] )



def create_grid( Nx=128, Ny=128, Nz=24, side=2., pa=0., ratio=1., ):

    x = np.linspace( -1, 1., Nx, ) * side / 2.
    y = np.linspace( -1, 1., Ny, ) * side / 2.
    #z = np.linspace( 0., 1., Nz, endpoint=True ) * side / 2.
    z = np.logspace( -3., 2., Nz, endpoint=True ) * side / 2.

    
    xx, yy, zz = np.meshgrid( x, y, z, sparse=False, indexing='ij' )
    rr = np.sqrt( xx**2 + yy**2 + zz**2 )
#   coord = np.array([xx,yy,zz,rr])
    
    parad = pa / 180. * np.pi
    rot = np.array( ([ np.cos(parad), np.sin(parad) ] , [ -np.sin(parad), np.cos(parad) ]) )
    x2 = np.array( [ xx[:,:,0], yy[:,:,0] ] ).T.dot( rot.T )
    pol_coord = np.sqrt( (x2[:,:,0]/ratio)**2 + (x2[:,:,1])**2 )

    return xx, yy, zz, rr, pol_coord



def read_data( path='./', flens=None, fbmap=None, fxtemp=None, fsz=None, ):

    out = []


    ###	Open strong lensing fits file and load the data into sl_obs
    if flens is not None :
        sl_obs = {}
        infile = path + flens
        hdulist1 = fits.open(infile)
        for hdu in hdulist1:
            sl_obs[hdu.name.lower()] = hdu.data
        sl_obs['mask'] = (sl_obs['mask'] == 1.)
        sl_obs['avg'] = sl_obs['avg'][sl_obs['mask']]
        if 'sigma' in sl_obs.keys():
            sl_obs['sigma'] = sl_obs['sigma'][sl_obs['mask']]
        else:
            try:
                sl_obs['fisher'] = sl_obs['fisher'][sl_obs['mask']][:,sl_obs['mask']]
            except IndexError:
                sl_obs['fisher'] = sl_obs['fisher'][sl_obs['mask'].flatten()][:,sl_obs['mask'].flatten()]
        hdulist1.close()
        out.append( sl_obs )



    ###	Open brightness map fits file and load the data into bmap_obs
    if fbmap is not None :
        bmap_obs = {}
        infile = path + fbmap
        hdulist1 = fits.open(infile)
        for hdu in hdulist1:
            bmap_obs[hdu.name.lower()] = hdu.data

        bmap_obs['scalebmap'] = hdulist1[0].header['scale']
        bmap_obs['bkg'] = hdulist1[0].header['bkg']
        bmap_obs['maskbmap'] = bmap_obs['maskbmap'] == 1.
        hdulist1.close()
        out.append( bmap_obs )


    ###	Open X-ray temperature fits file and load the data into bmap_obs
    if fxtemp is not None:
        xtemp_obs = Table.read( path + fxtemp )
        out.append( xtemp_obs )


    ###Open Sunyaev-Zeldovich fits file and load the data into sz_obs
    if fsz is not None :
        sz_obs = {}
        infile = path + fsz
        hdulist1 = fits.open(infile)
        for hdu in hdulist1:
            sz_obs[hdu.name.lower()] = hdu.data
        sz_obs['scale'] = hdulist1[0].header['scale']
        sz_obs['mask'] = (sz_obs['mask'] == 1.)
        hdulist1.close()
        out.append( sz_obs )

    return out



def load_chain( dbnamein ):
    """
    Open file and load data into inp
    """

    infile = open(dbnamein,'rb')
    inp = pickle.load( infile )
    infile.close()
    return inp


def initial_pos( model, dbnamein ):

    inp = load_chain( dbnamein )
    chain = inp['chain'][0]
    labels = inp['tofit']
    if 'rs' in labels:
        labels[ labels.index('rs') ] = 'r200'

    variables = [ v.name for v in model.variables ]
    
    try:
        acor = emcee.autocorr.integrated_time( np.mean( chain, axis=0 ), c=1 )
    except emcee.autocorr.AutocorrError:
        print( 'Chain too short!' )
        acor = np.zeros( len(labels) )

    burn = int( np.min( ( 0.9 * chain.shape[1], 10 * np.max(acor) ) ) )
    for i,p in enumerate( labels ):
        if p in variables:
            model.__dict__[p].itemset( np.median( chain[ :, burn:, i ] ) )




def FlatPrior(p):
	"""
		Flat Prior
	"""
	if p['Lower'] <= p['Value'] <= p['Upper']:
		return 0.
	else:
		return -np.inf


def GaussianPrior(p):
	"""
		Gaussian Prior
	"""
	if p['Lower'] <= p['Value'] <= p['Upper']:
		return -0.5*((p['Value'] - p['Mean'])/p['Sigma']/2.)**2
	else:
		return -np.inf
		
class lnprior(object):
    
    def __init__( self, tpar ):
        self.tpar = tpar
		
    def __call__( self, pos ):
        '''
        Log of priors
        '''
        tpar = self.tpar
        
        lp = 0
        for p in tpar.items():
            if not p[1]['Observed']:
                p[1]['Value'] = pos[p[1]['Pos']]
                try:
                    lp += p[1]['Prior'](p[1]['Value'])
                except KeyError:
                    lp += scipy.stats.uniform.logpdf(
                        p[1]['Value'],
                        loc=p[1]['Lower'],
                        scale=(p[1]['Upper']-p[1]['Lower']),
                        )
	
                if not np.isfinite(lp):
                    return -np.inf

        return lp
