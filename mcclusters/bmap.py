import numpy as np
import subprocess
import os
from astropy.io import fits

def xspec(nh,temp,abun,redsh,file_rmf,file_arf,fuf,fmodel):

    try:
	    model = open(fmodel).read()
	    model = model.replace('temp',str(temp))
	    model = model.replace('abundance',str(abun))
	    model = model.replace('nh',str(nh))
	    model = model.replace('redsh',str(redsh))
    except FileNotFoundError:
	    model = 'cosmo 70. .3 .7\n\
abund grsa\n\
model tbabs*edge(mekal)\n\
    '+str(nh)+' -1\n\
    2.07 -1\n\
    -0.15 -0.01 -1 -1 2 2\n\
    '+str(temp)+' 0.01 0.01 0.01 79.9 79.9\n\
    1.0\n\
    '+str(abun) + ' 1.e-2\n\
    '+str(redsh)+'\n\
    0.0\n\
    1.0\n\
###\n\
newpar 1 0; newpar 2 0; newpar 3 0\n'

#ignore **-0.7 2.-**\n\

    x_script = '\
autosave off\n\
'+model.split('###\n')[0]+'\
fakeit none\n\
'+file_rmf+'\n\
'+file_arf+'\n\
n\n\
fuf\n\
'+fuf+'\n\
1.0e9\n\
'+model.split('###\n')[-1]+'\
exec echo aloa 1; lumin '+str(0.7)+' '+str(2.)+' '+str(redsh)+'\n\
exec echo aloa 4; dummyrsp 0.7 2. 1000 log; lumin '+str(0.01)+' '+str(100.)+' '+str(redsh)+'\n\
show;\n\
exit\n\
y'
#    print(x_script)

    try:
        os.remove(fuf)
    except OSError:
	    pass
    p1 = subprocess.Popen(["xspec"],stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    fuf1 = str(p1.communicate(x_script.encode('utf-8'))[0])

#    out = []
#    for lum in fuf1.split('Model Luminosity')[1:] : out.append(float(lum.split()[0]))

#    return out
    return 0,1


def sim_spec(path,nh,redsh,rmf,arf, fmodel='model_bmap.xcm'):

    print('Running simulation of the spectra...')

    try :
        open(fmodel).read()
    except FileNotFoundError:
        print('Model file not found. Using default')

    #	nh = 0.022
    #	redsh = 0.539
    #	rmf = '/home/Software/Fulltriax/responses/aciss_aimpt_cy13.rmf'
    #	arf = '/home/Software/Fulltriax/responses/aciss_aimpt_cy13.arf'
    fuf = path+'fuf.pha'

    ktsim = np.append([0.01,0.05,0.1,0.3,0.6],np.exp((np.arange(40))/11.5))
    lam = np.zeros([len(ktsim),2,2])
    spec = np.zeros([len(ktsim),2,1024])
    for k1 in range(len(ktsim)):

    #		abundance 0
	    #        c_crate_spec11b,file_rmf,file_arf,[0.5,5.0],nh,z,0.,ktsim[k1],lum,lum_bol
	    lum,lum_bol = xspec(nh,ktsim[k1],0.,redsh,rmf,arf,fuf, fmodel=fmodel)
	    lam[k1,0,0] = lum
	    lam[k1,0,1] = lum_bol
	    #        readpha,count,counterr,rate,back,'fuf.pha'
	    count = fits.getdata(fuf)['rate']
	    spec[k1,0,:] = count #/ 1e9

	    #		abundance 1
	    lum,lum_bol = xspec(nh,ktsim[k1],1.,redsh,rmf,arf,fuf, fmodel=fmodel)
	    #        c_crate_spec11b,file_rmf,file_arf,[0.5,5.0],nh,z,1.,ktsim[k1],lum,lum_bol
	    lam[k1,1,0] = lum - lam[k1,0,0]
	    lam[k1,1,1] = lum_bol - lam[k1,0,1]
	    #        readpha,count,counterr,rate,back,'fuf.pha'
	    count = fits.getdata(fuf)['rate']
	    spec[k1,1,:] = ( count - spec[k1,0,:] )
#	    spec[k1,1,:] = ( count /1e9 - spec[k1,0,:] )

    return spec
    #    save,filename='lambda.sav',nh,z,lam,spec,ktsim


def cout( spec_tmp, tmap, Ab ):
#	restore,'lamlsbda.sav'
#	36-->342 0.5:5.0 keV
#	t -> output rings, ktsim -> simulated rings      NEED to inter/extrapolate
#	spec_tmp = spec[:,:,49:137].sum(axis=2)   # 0.7 - 2
#	spec_tmp = spec[:,:,35:341].sum(axis=2)   # 0.5 - 5
	
#	print('Retrieving counts from simulation of the spectra')

	ktsim = np.append([0.01,0.05,0.1,0.3,0.6],np.exp((np.arange(40))/11.5))
	ttemp = np.reshape(ktsim.repeat(tmap.size),[ktsim.size,tmap.size]).T - np.reshape(tmap.repeat(ktsim.size),[tmap.size,ktsim.size])
	ktmin = np.argmin(abs(ttemp),axis=1)
	sgnT = np.sign(tmap-ktsim[ktmin])

#   0.5 - 5.0
#	36-->342
#   35 343
#   35:341


#   
#   48 137
#   48:135
	ktsign = np.maximum(ktmin+sgnT,0).astype(int)
	cout = (spec_tmp[ktmin,0]+Ab*spec_tmp[ktmin,1]) + sgnT*((spec_tmp[ktsign,0]+Ab*spec_tmp[ktsign,1])-(spec_tmp[ktmin,0]+Ab*spec_tmp[ktmin,1]))\
	   /np.maximum(abs(ktsim[ktsign.astype(int)]-ktsim[ktmin.astype(int)]),1e-4)*(tmap-ktsim[ktmin.astype(int)])

#	spec_tmp = lam[:,:,1]
#	lum4 = (spec_tmp[ktmin,0]+Ab*spec_tmp[ktmin,1]) + sgnT*((spec_tmp[ktsign,0]+Ab*spec_tmp[ktsign,1])-(spec_tmp[ktmin,0]+Ab*spec_tmp[ktmin,1]))\
#	   /np.maximum(abs(ktsim[ktsign.astype(int)]-ktsim[ktmin.astype(int)]),1e-4)*(tmap-ktsim[ktmin.astype(int)])

#	spec_tmp = lam[:,:,0]
#	lum1 = (spec_tmp[ktmin,0]+Ab*spec_tmp[ktmin,1]) + sgnT*((spec_tmp[ktsign,0]+Ab*spec_tmp[ktsign,1])-(spec_tmp[ktmin,0]+Ab*spec_tmp[ktmin,1]))\
#	   /np.maximum(abs(ktsim[ktsign.astype(int)]-ktsim[ktmin.astype(int)]),1e-4)*(tmap-ktsim[ktmin.astype(int)])

	return cout#,lum4,lum1
	
	
	
	
	
	
	
	
