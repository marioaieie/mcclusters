import numpy as np
import scipy.integrate
from astropy import units
import astropy.constants as cn

from ..variable import variable
from scipy.stats import uniform, truncexpon

class SphericalNFW(object):

    def __init__( self, coord=None, tscale=1., spec=None, abring=None ):
        
        self.variables = []
        
        self.halo = halo( coord )
        for v in self.halo.variables:
            self.__dict__[ v.name ] = v

        self.coord = self.halo.coord
        self.dm2d = self.halo.dm2d
        self.variables = self.variables + self.halo.variables



        self.gas = gas( self.halo, tscale, spec, abring )
        for v in self.gas.variables:
            self.__dict__[ v.name ] = v
            
        self.em2d = self.gas.em2d
        self.t2d = self.gas.t2d
        self.dt2d = self.gas.dt2d
        self.variables = self.variables + self.gas.variables
        
        
    def update( self, ):
        self.halo.update()
        self.gas.update()


#    def em2d( self, ):
#         return self.gas.em2d()

#    def t2d( self, ):
#        return self.gas.t2d( )
#        
#    def dt2d( self, ):
#        return self.gas.dt2d( )


class halo( object ):
 
    # scipy.stats.uniform wants arguments loc and scale so that:
    # This distribution is constant between `loc` and ``loc + scale``.
    variables = [ 
        variable( 'c', 5.18, prior=uniform( 1., 19. ) ),
        variable( 'r200', 1871., prior=uniform( 600., 3000. ) ),
        ]

    def __init__( self, coord ):
        
        for v in self.variables:
            self.__dict__[ v.name ] = v

        self.coord = coord
        #self.update()
    

    def update( self, ):

        self.aa, self.bb, self.cc =  self.coord[:-1]

        self.deltac = 200./3 * self.c**3 / self.fx( self.c )
        self.rs = self.r200 / self.c
        

    def dm2d( self, ):
        rdm = self.coord[3]
        dm3d = self.deltac / ( rdm / self.rs ) \
               / ( 1. + rdm / self.rs )**2.
#        dm2d = scipy.integrate.simps( dm3d, self.halo.coord[2] )
        dm2d = scipy.integrate.simps( dm3d, self.coord[2] )
        dm2d = dm2d + dm2d[::-1,::-1]
        
        return dm2d
        
    def mr( self, r ):
        return self.rs**3 * self.deltac * self.fx( r / self.rs )
        
    def fx( self, x ):
        return np.log( 1 + x ) - x / ( 1 + x )
        


class gas( object ):
    # scipy.stats.uniform wants arguments loc and scale so that:
    # This distribution is constant between `loc` and ``loc + scale``.

    variables = [         
        variable( 'n0', 0.011, prior=uniform( 1e-6, 1.-1e-6 ) ),
        variable( 'rc1', 90.3, prior=uniform( 10., 290. ) ),
        variable( 'epsilon', 0.425, prior=uniform( 0., 1. ) ),
        variable( 'delta', 0., prior=uniform( 0., 1. ) ),
        variable( 'Pnt', 0., prior=truncexpon(10., scale=0.1) ),
        variable( 'P0', 1., prior=uniform( 0., 10.-1e-3 ) ),

        variable( 'n', 0., prior=uniform( 0., 1. ) ),
        variable( 'rc2', 702., prior=uniform( 300., 1700. ) ),
        variable( 'nu', 1.05, prior=uniform( 0., 4. ) ),
        variable( 'nu0', 3., prior=uniform( 0.1, 4. ) ),
    ]

    def __init__( self, halo, tscale, spec, abring ):
    
        self.halo = halo
        self.tscale = tscale
        self.spec = spec
        self.abring = abring
        
        self._theta = (units.keV / (cn.m_e * cn.c**2 )).si.value


        for v in self.variables:
            self.__dict__[ v.name ] = v            

        #self.update()
    
    
    def update( self ):
        self.nele3d = None
        self.t3d = None
        self.ricm = self.halo.coord[3]


#    def _set_nele3d( self ):

#        #print('Computing nele3d')

#        e_dm_s = np.sqrt( 1. - self.halo.s**2 )
#        e_dm_q = np.sqrt( 1. - self.halo.q**2 )


#        self.x = np.exp( 2.02 * np.arange(100)**0.3 )
#        u = self.x / self.halo.rs / self.halo.s


#        int1 = np.array( [ scipy.integrate.quad( self.int_suto1, 0., ut, epsrel=1e-2 )[0]
#                         for ut in u ] )
#        int2 = np.array( [ scipy.integrate.quad( self.int_suto2, 0., ut, epsrel=1e-2 )[0]
#                         for ut in u ] )
#        int3 = np.array( [ scipy.integrate.quad( self.int_suto3, 0., ut, epsrel=1e-2 )[0]
#                         for ut in u ] )

#        F1 = 1. / ( self.halo.alpha - 2. ) * ( 1. - 1. / u * int1 )
#        F2 = 1. / ( self.halo.alpha - 2. ) * ( -2. / 3. + 1. / u * int1 - 1. / u**3 * int2 )
#        F3 = 1. / u**3 * int3

#        fac = np.sqrt( ( self.halo.alpha - 2. ) * F3 / ( 1. - ( self.halo.alpha - 2. ) * F1 \
#                       - u**( 2. - self.halo.alpha ) * ( 1. + u )**( self.halo.alpha - 2. ) ) )
#        fs = 1. + ( 0.1 + 0.09 * np.log10( 1 + u ) ) * e_dm_q**3 \
#             + ( 0.2 + 0.03 * np.log10( 1 + u ) ) * e_dm_s**3
#        fq = 1. + ( 0.1 + 0.05 * np.log10( 1 + u ) ) * e_dm_s**3 \
#             + ( 0.2 + 0.03 * np.log10( 1 + u ) ) * e_dm_q**3
#        e_gas_s = fac * e_dm_s * fs
#        e_gas_q = fac * e_dm_q * fq

#        self.Phi = self.halo.deltac * self.halo.rs**2 * ( F1 + ( e_dm_q**2 + e_dm_s**2 ) / 2. * F2 )


#        self.nele3d = self.nele( self.ricm )
##        self.t3d = None


    def em2d( self, ):
        if self.nele3d is None:
            self.nele3d = self.nele( self.ricm )

        if self.spec is not None :
            if self.t3d is None:
                self._set_t3d()
            cool = self.cout( self.t3d.flatten() ).reshape( self.nele3d.shape )
        else:
            cool = 1.

        em2d = scipy.integrate.simps( cool * self.nele3d**2, self.halo.coord[2] )
        return  em2d + em2d[::-1,::-1]

    def fdphi( self, y,  r ):
        return - self.tscale * self.nele( r ) * self.halo.mr( r ) / r**2 
       
    def _set_t3d( self, ):

        #print('Computing t3d')

        if self.nele3d is None:
            self.nele3d = self.nele( self.ricm )

        
        #rref = np.linspace( 10., 1.1*self.halo.coord[2][-1,-1,-1], 50 )
        rref = np.logspace( 0, np.log10( self.halo.coord[0][-1,-1,-1]), 50 )

        pres0 = self.P0 * self.nele( rref[-1] )
        P = scipy.integrate.odeint( self.fdphi, pres0, rref[::-1] )[::-1,0]
        t = P / self.nele( rref )

#        t = np.minimum( 20., t )
#        t = np.maximum( 0., t )
        self.t3d = np.exp( np.interp( np.log(self.ricm), np.log(rref), np.log(t) ) )
        
        

    def t2d( self, ):
    
        if self.t3d is None:
            self._set_t3d()

        tw = self.nele3d**2 * self.t3d**-0.75
        return scipy.integrate.simps( self.t3d * tw, self.halo.coord[2] ) \
               / scipy.integrate.simps( tw, self.halo.coord[2] )


    def dt2d( self, ):
    
        if self.t3d is None:
            self._set_t3d()
        dt2d = scipy.integrate.simps( self.nele3d * self.t3d * self.Dfsz( self.t3d ), self.halo.coord[2] )
        return  dt2d + dt2d[::-1,::-1]

    def Dfsz( self, T ):
        theta = T * self._theta
        D_SZ = - 17. / 10. * theta + 123. / 40. * theta**2 \
               - 1989. / 280. * theta**3 + 14403. / 640. * theta**4
        return 1 + D_SZ

    def fsz( self, nu, Tcmb=2.725 *units.K ):
        x = cn.h * nu / ( cn.k_B * Tcmb )
        return ( x * ( np.exp(x) + 1. ) / (np.exp(x) - 1. ) - 4. )


    def nele( self, r ):
        return self.n0 * ( r / self.rc1 )**( -self.delta ) \
        * ( 1. + ( r / self.rc1 )**2. )**( - 1.5 * self.epsilon + self.delta / 2. ) \
        * ( 1. + ( r / self.rc2 )**self.nu0 )**( -self.nu / self.nu0 )

    def int_suto1( self, t ):
        return ( t / ( t + 1. ) )**( 2. - self.halo.alpha )
    def int_suto2( self, t ):
        return t**( 4. - self.halo.alpha ) / ( t + 1. )**( 2. - self.halo.alpha )
    def int_suto3( self, t ):
        return t**( 4. - self.halo.alpha ) / ( t + 1. )**( 3. - self.halo.alpha )
    
    def cout( self, tmap ):
    #####   0.5 - 5.0
    #   	36-->342
    #       35 343
    #       35:341
    #####   0.7 - 2
    #       48 137
    #       48:135

	    ktsim = np.append([0.01,0.05,0.1,0.3,0.6],np.exp((np.arange(40))/11.5))
	    ttemp = np.reshape(ktsim.repeat(tmap.size),[ktsim.size,tmap.size]).T - np.reshape(tmap.repeat(ktsim.size),[tmap.size,ktsim.size])
	    ktmin = np.argmin(abs(ttemp),axis=1)
	    sgnT = np.sign(tmap-ktsim[ktmin])

	    ktsign = np.maximum(ktmin+sgnT,0).astype(int)
	    self.spec = self.spec  #[:,:,49:137].sum(axis=2)
    #	self.spec = spec[:,:,35:341].sum(axis=2)
	    cout = (self.spec[ktmin,0]+self.abring*self.spec[ktmin,1]) + sgnT*((self.spec[ktsign,0]+self.abring*self.spec[ktsign,1])-(self.spec[ktmin,0]+self.abring*self.spec[ktmin,1]))\
	       /np.maximum(abs(ktsim[ktsign.astype(int)]-ktsim[ktmin.astype(int)]),1e-4)*(tmap-ktsim[ktmin.astype(int)])

	    return cout


