import numpy as np
import scipy.integrate
from scipy.stats import uniform
from ..variable import variable

tpar = {'A':{'Observed':True, 'Value': 1., 'Lower': -2., 'Upper': 1.},\
        's':{'Observed':True, 'Value': 0.6, 'Lower': 0.1, 'Upper': 1.},
        'alpha':{'Observed':True, 'Value': -2, 'Lower': -3., 'Upper': -1.},
        }


class toy(object):

    # scipy.stats.uniform wants arguments loc and scale so that:
    # This distribution is constant between `loc` and ``loc + scale``.
    variables = [
        variable( 'alpha', -2., prior=uniform( -3., 2. ) ),
        variable( 's', 0.6, prior=uniform( 0.1, 0.9 ) ),
        variable( 'A', 1., prior=uniform( 0., 3. )  ),
        ]
    
    def __init__( self, coord ):

        for v in self.variables:
            self.__dict__[ v.name ] = v
        
        self.coord = coord
        self.update()


    def update( self, ):
    
        self.rho3d = self._rho3d()
        
        

    def _rho3d( self ):
        r2 = ( self.coord[0]**2 + self.coord[1]**2 ) / self.s**2 + self.coord[2]**2 
        return self.A * np.sqrt( r2 )**self.alpha


    def dm2d( self ):
        return scipy.integrate.simps( self.rho3d )
        
    def sb2d( self ):
        return scipy.integrate.simps( self.rho3d**2 )
