import numpy as np

class variable(np.ndarray):

    def __new__(cls, name, input_array, prior ):
        """
            name: name of the parameter
            prior: prior distrution (e.g. scipy.stats.uniform). The object needs to have implemented
                   a logpdf method. See scipy.stats random variable distribution manual for more details.
        """

        assert prior is None or hasattr( prior, 'logpdf' ), "Prior object does not have logpdf method."

        obj = np.asarray(input_array).view(cls)
        obj.name = name
        obj.prior = prior
        return obj

    def __array_finalize__(self, obj):
        
        if obj is None: return
        self.name = getattr(obj, 'name', None)
        self.prior = getattr(obj, 'prior', None )


    def __reduce__(self):
        # Get the parent's __reduce__ tuple
        pickled_state = super( variable, self).__reduce__()

        # Create our own tuple to pass to __setstate__
        new_state = pickled_state[2] + ( self.name, self.prior, )

        # Return a tuple that replaces the parent's __setstate__ tuple with our own
        return (pickled_state[0], pickled_state[1], new_state)

    def __setstate__(self, state):
  
        self.name = state[-2]  # Set the name attribute
        self.prior = state[-1]  # Set the prior attribute
  
        # Call the parent's __setstate__ with the other tuple elements.
        super( variable, self).__setstate__( state[0:-2] )

