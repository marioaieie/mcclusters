import copy
import numpy as np
from .variable import variable
from scipy.stats import uniform, truncexpon

def m1423():
    variables = [ 
        variable( 'c', 3.97, prior=uniform( 1., 19. ) ),
        variable( 'r200', 3.97 * 644.7, prior=uniform( 100., 1900. ) ),
        variable( 'alpha', 1.06, prior=uniform( 0., 3. ) ),

        variable( 's', 0.62, prior=uniform( 0.1, 0.9 ) ),
        variable( 'q_scaled', 0.26, prior=uniform( 0., 1.0 ) ),   
        variable( 'psi', (-72.3/180.*np.pi), prior=uniform( -np.pi, 2*np.pi ) ),
        variable( 'phi', (-34.7/180.*np.pi), prior=uniform( -np.pi, 2*np.pi ) ),
        variable( 'theta', (34.4/180.*np.pi), prior=uniform( -0.1, np.pi + 0.1 ) ),

        variable( 'n0', 0.15, prior=uniform( 1e-6, 1.-1e-6 ) ),
        variable( 'rc1', 20.6, prior=uniform( 10., 290. ) ),
        variable( 'epsilon', 0.55, prior=uniform( 0., 1. ) ),
        variable( 'delta', 0.02, prior=uniform( 0., 1. ) ),
        variable( 'Pnt', 0.08, prior=truncexpon(10., scale=0.1) ),
        variable( 'P0', 0.8, prior=uniform( 1e-3, 10.-1e-3 ) ),

        variable( 'n', 0., prior=uniform( 0., 1. ) ),
        variable( 'rc2', 600., prior=uniform( 300., 1700. ) ),
        variable( 'nu', 0., prior=uniform( -1., 2. ) ),
    ]

    return variables


def a1703():
    variables = [ 
        variable( 'c', 5.038, prior=uniform( 1., 12. ) ),
        variable( 'r200', 1803., prior=uniform(  5e2, 3e3 ) ),
        variable( 'alpha', 1., prior=uniform( 0., 3. ) ),

        variable( 's', 1., prior=uniform( 0.1, 0.9 ) ),
        variable( 'q_scaled', 1., prior=uniform( 0.1, 0.9 ) ),   
        variable( 'psi', 0., prior=uniform( -np.pi, 2*np.pi ) ),
        variable( 'phi', 0., prior=uniform( -np.pi, 2*np.pi ) ),
        variable( 'theta', 0., prior=uniform( -0.1, np.pi + 0.1 ) ),

        variable( 'n0', 0.00754953535352, prior=uniform( 1e-3, 0.3 ) ),
        variable( 'rc1', 107.667742183, prior=uniform( 10., 290. ) ),
        variable( 'epsilon', 0.46643838044, prior=uniform( 0., 1. ) ),
        variable( 'delta', 0., prior=uniform( 0., 1. ) ),
        variable( 'Pnt', 0., prior=truncexpon(10., scale=0.1) ),
        variable( 'P0', 0.6, prior=uniform( 1e-2, 1e2 ) ),

        variable( 'n', 0., prior=uniform( 0., 1. ) ),
        variable( 'rc2', 587.836284708, prior=uniform( 300., 1700. ) ),
        variable( 'nu', 0.708435460847, prior=uniform( 0., 4. ) ),
    ]

    return variables


def test():
    variables = [ 
        variable( 'c', 4., prior=uniform( 1., 19. ) ),
        variable( 'r200', 4.*600., prior=uniform( 5e2, 1e4 ) ),
        variable( 'alpha', 1., prior=uniform( 0., 3. ) ),

        variable( 's', 0.6, prior=uniform( 0.1, 0.9 ) ),
        variable( 'q_scaled', 0.5, prior=uniform( 0.1, 0.9 ) ),   
        variable( 'psi', -1.2, prior=uniform( -np.pi/2, np.pi ) ),
        variable( 'phi', -0.6, prior=uniform( -np.pi, 2*np.pi ) ),
        variable( 'theta', 0.5, prior=uniform( -0.1, np.pi + 0.1 ) ),

        variable( 'n0', 0.1, prior=uniform( 1e-3, 1. ) ),
        variable( 'rc1', 30., prior=uniform( 10., 290. ) ),
        variable( 'epsilon', 0.55, prior=uniform( 0., 1. ) ),
        variable( 'delta', 0., prior=uniform( 0., 1. ) ),
        variable( 'Pnt', 0., prior=truncexpon(10., scale=0.1) ),
        variable( 'P0', 6., prior=uniform( 1e-3, 10.-1e-3 ) ),

        variable( 'n', 0., prior=uniform( 0., 1. ) ),
        variable( 'rc2', 600., prior=uniform( 300., 1700. ) ),
        variable( 'nu', 0., prior=uniform( -1., 2. ) ),
    ]

    return variables

