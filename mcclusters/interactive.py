import matplotlib.pyplot as plt
import numpy as np
import scipy.stats
import fulltriax_emcee as ft
from matplotlib.widgets import Slider
from matplotlib.widgets import Button
import colormaps as cmaps

from mcclusters.models.sutolab import tpar
import mcclusters.models.sutolab as model




import scipy.stats
pa = ft.pa
ratio = ft.ratio
parad = (-pa)/180.*np.pi
rot = np.array([ [ np.cos(parad), -np.sin(parad) ] , [ np.sin(parad), np.cos(parad) ] ])

sx,sy = np.meshgrid(ft.sl_obs['x'],ft.sl_obs['y'], sparse=False, indexing='xy')
x2 = np.array([sx,sy]).T.dot(rot.T)
#rrs =  np.sqrt( (x2[:,:,0]/ratio)**2 + (x2[:,:,1])**2 )
rrs = np.sqrt( sx**2 + sy**2 )
binsSL = np.logspace( np.log10( np.min( np.abs( ft.sl_obs['x'] ) ) ), np.log10( ft.sl_obs['x'].max()), 20 )

mask = (ft.bmap_obs['maskbmap']==1).flatten()
bx,by = np.meshgrid(ft.bmap_obs['xbmap'],ft.bmap_obs['ybmap'], sparse=False, indexing='xy')
#x2 = np.array([bx,by]).T.dot(rot.T)
#rrx =  np.sqrt( (x2[:,:,0]/ratio)**2 + (x2[:,:,1])**2 )
rrx = np.sqrt(bx**2+by**2).T
#rr = sqrt(xx[:,:,0]**2+yy[:,:,0]**2)
#figure()
#plot(np.log10(rrx.flatten()),bmap_obs['imhbmap'].flatten(),',')

bmap_dat = ft.bmap_obs['imhbmap'].flatten()[mask]
sigx = (1. + np.sqrt( bmap_dat + 0.75 ))
Mbmap = ft.bmap_obs['k']*(ft.bmap_obs['scalebmap']*ft.units.kpc.to("cm"))**2 *ft.dz.to('cm').value
bins = np.logspace( np.log10( np.min( np.abs( ft.bmap_obs['xbmap'] ) ) ), np.log10( ft.bmap_obs['xbmap'].max()), 20 )


sprof = scipy.stats.binned_statistic( rrs.flatten(), ft.sl_obs['avg'].flatten(),
                                     bins=binsSL )
ssig = scipy.stats.binned_statistic( rrs.flatten(), ft.sl_obs['fisher'].flatten(),
                                     bins=binsSL )


bsig = scipy.stats.binned_statistic( rrx.flatten()[mask], sigx, bins=bins, statistic='mean' )
weigth = scipy.stats.binned_statistic( rrx.flatten()[mask], 1./sigx, bins=bins, statistic='sum' )
Nbin = np.histogram( rrx.flatten(), bins=bins )


bprof = scipy.stats.binned_statistic( rrx.flatten()[mask], bmap_dat / sigx, bins=bins, statistic='sum' )
bprof2 = scipy.stats.binned_statistic( rrx.flatten()[mask], bmap_dat, bins=bins, statistic='mean' )

        
for v,p in zip( ft.fitted, ft.pos0 ):
    v.itemset( p )

ft.model_fit.update()

dm2d = ft.lens._rescale( ft.model_fit.dm2d() )
tfit = ft.xt._profile()

em2d = ft.sb._rescale( ft.model_fit.em2d() ) 

#bmap.append( em2d * ft.bmap_obs['scale'] + ft.bmap_obs['bkg'])
#smap.append(dm2d)
bmap_mod = ( em2d * ft.bmap_obs['scale'] + ft.bmap_obs['bkg'] ).flatten()
bfit = scipy.stats.binned_statistic( rrx[ft.bmap_obs['maskbmap']].flatten(), bmap_mod,
                                  bins=bins, statistic='mean')[0] 
sfit = scipy.stats.binned_statistic( rrs.flatten()[ft.sl_obs['mask']],
                                          dm2d.flatten() * ft.slscale,
                                          bins=binsSL )[0] 

slt = scipy.stats.binned_statistic( rrs.flatten()[ft.sl_obs['mask']], dm2d.flatten() * ft.slscale, bins=binsSL )[1]
rsl = slt[:-1] + np.diff(slt) / 2
sbt = scipy.stats.binned_statistic( rrx[ft.bmap_obs['maskbmap']].flatten(), bmap_mod, bins=bins, statistic='sum' )[1]
rsb = sbt[:-1] + np.diff(sbt) / 2



alpha = 1.
cmc = 'b'
ctr = 'r'
ctn = 'g'

ax = [1]*3



figSL, ax[0] = plt.subplots()

#r2 = np.array([rsl,]).repeat(sfit.shape[1], axis=0).T
lineSL, = ax[0].plot(rsl, sfit, color=cmc, alpha=alpha, label='MCMC realisations');

rprof = sprof[1][:-1]+np.diff(sprof[1])/2
ax[0].errorbar( rprof, sprof[0] , 
               yerr=ssig[0],fmt='g.-', label='Observed')
ax[0].legend(loc=1,numpoints=1)
ax[0].set_ylabel('$\Sigma \;$[ g/cm$^2$]')
ax[0].loglog()



figSB, ax[1] = plt.subplots()

lineSB, = ax[1].plot( rsb, bfit, alpha=alpha, color=cmc );
ax[1].errorbar( bprof[1][:-1]+np.diff(bprof[1])/2 , bprof2[0],
               yerr=bsig[0]/np.sqrt(Nbin[0]), fmt=ctn+'-', label='True + noise')

ax[1].legend(loc=1,numpoints=1)
ax[1].loglog()
ax[1].set_xlabel('R [kpc]')
ax[1].set_ylabel('$S_X \;$[ cts / s ]')



fig, ax[2] = plt.subplots()

line, = ax[2].plot( ft.Xtemp_obs['r'], tfit, color=cmc, alpha=alpha)
ax[2].plot((), color=cmc, alpha=alpha, label='MCMC realisations');

ax[2].errorbar(ft.Xtemp_obs['r'], ft.Xtemp_obs['kt'], yerr=ft.Xtemp_obs['ekt'], fmt='gs-', label='Observed')

ax[2].set_ylim(3.6,9.)

ax[2].legend(loc=1,numpoints=1)
ax[2].set_ylabel('$T \;$[ keV ]')
ax[2].set_xlabel('$R \;$[ kpc ]')
ax[2].semilogx()


for a in ax:
    a.set_xlim(25.,2600.)
    a.set_xlabel('R [kpc]')




def on_change(val):
    for sldr in sliders:
    #			print(sldr.label.get_text(),tpar[sldr.label.get_text()]['Observed'],sldr.val)
        ft.model_fit.__dict__[sldr.label.get_text()].itemset( sldr.val )
		

    ft.model_fit.update()

    dm2d = ft.lens._rescale( ft.model_fit.dm2d() )
    tfit = ft.xt._profile()

    em2d = ft.sb._rescale( ft.model_fit.em2d() ) 

    #bmap.append( em2d * ft.bmap_obs['scale'] + ft.bmap_obs['bkg'])
    #smap.append(dm2d)
    bmap_mod = ( em2d * ft.bmap_obs['scale'] + ft.bmap_obs['bkg'] ).flatten()
    bfit = scipy.stats.binned_statistic( rrx[ft.bmap_obs['maskbmap']].flatten(), bmap_mod,
                                      bins=bins, statistic='mean')[0] 
    sfit = scipy.stats.binned_statistic( rrs.flatten()[ft.sl_obs['mask']],
                                              dm2d.flatten() * ft.slscale,
                                              bins=binsSL )[0] 


    lineSL.set_ydata( sfit )
    lineSB.set_ydata( bfit )
    line.set_ydata( tfit )


    figSL.show()
    figSB.show()
    fig.show()



#plt.figure()
#sliders = []
#i = 0
#for par in tpar.items():
##	if (par[1]['Observed']>= 0):
#	ax2 = plt.axes([0.1, 0.05+0.05*i, 0.8, 0.02])
#	sliders.append(Slider(ax2, par[0], par[1]['Lower'], par[1]['Upper'], valinit=par[1]['Value'], color='#AAAAAA'))
#	sliders[-1].on_changed(on_change)
#	i += 1


plt.figure()
sliders = []
i = 0
for var in ft.model_fit.variables:
    ax2 = plt.axes([0.1, 0.05+0.05*i, 0.8, 0.02])
    sliders.append(Slider(ax2, var.name, *var.prior.interval(1), 
                          valinit=var, color='#AAAAAA'))
    sliders[-1].on_changed(on_change)
    i += 1

ax2 = plt.axes([0.2, 0.05+0.05*i, 0.6, 0.1])
def reset_tpar(val):
    for sldr in sliders: sldr.reset()

but = Button(ax2,'Reset')
but.on_clicked(reset_tpar)




#def on_change(val):
#    for var,sldr in zip(sutolab.variables,sliders):
#        var.itemset( sldr.val )
#    
#    sutolab.update()
#    ax.imshow( np.log10(sutolab.em2d()), interpolation='none', cmap='winter')
#    fig.show()
















#lnlike, (chi2 ,(dm2d, emt, sb_fit, tfit_x0)) = mcclusters.lnprob(pos0,*args)

##args = (model, 'SBT',tpar,coord,pol_coord,sl_obs,bmap_obs,Xtemp_obs,slscale,pscale,tscale, 
##ratio, pa, True)


#print( pos0, pa, ratio)



#parad = (-pa)/180.*np.pi
#rot = np.array([ [ np.cos(parad), -np.sin(parad) ] , [ np.sin(parad), np.cos(parad) ] ])



#sx,sy = np.meshgrid(sl_obs['x'],sl_obs['y'], sparse=False, indexing='xy')
#x2 = np.array([sx,sy]).T.dot(rot.T)
#rrs =  np.sqrt( (x2[:,:,0]/ratio)**2 + (x2[:,:,1])**2 )
##rrs =  np.sqrt( sx**2 + sy**2 )

##sbins = np.logspace( 2., np.log10( 2860. ), 20 )
#sbins = np.logspace( np.log10( np.min( np.abs( sl_obs['x'] ) ) ),
#                     np.log10( sl_obs['x'].max()), 10 )

#sprof = scipy.stats.binned_statistic(
#    rrs.flatten()[ sl_obs['mask'] ], sl_obs['avg'].flatten(),
#    bins=sbins
#    )
#sfit = scipy.stats.binned_statistic(
#    rrs.flatten()[sl_obs['mask']],
#    dm2d.flatten()[sl_obs['mask']] * slscale,
#    bins=sbins
#    )

#figSL, axSL = plt.subplots()
#axSL.plot( sprof[1][:-1] + np.diff(sprof[1])/2, sprof[0],'rs-')
#lineSL, = axSL.plot( sfit[1][:-1]+np.diff(sfit[1])/2,sfit[0],'b-')
#textSL = axSL.text(0.85,0.9,"%.2f"%(chi2[0]),transform=axSL.transAxes)

#figSL.show()

##figSL2, axSL2 = plt.subplots()
##ims = axSL2.imshow(dm2d*slscale,origin='lower',interpolation='none',cmap=cmaps.viridis)#\
###	              vmin=-0.5,)#vmax=0.5,)
##figSL2.colorbar(ims)

##figSL3, axSL3 = plt.subplots()
##ims = axSL3.imshow( ( dm2d * slscale - sl_obs['avg'].reshape(dm2d.shape) )**2 \
##                    / sl_obs['fisher'].reshape(dm2d.shape)**2,
##                    origin='lower',interpolation='none',
##                   extent=[sl_obs['x'][0],sl_obs['x'][-1],sl_obs['x'][0],sl_obs['x'][-1]],
###                   vmin=-0.5,vmax=0.5,\
##                   cmap=cmaps.viridis)
##figSL3.colorbar(ims)
##figSL3.show()


#sblim = Xtemp_obs['r'][-1]
#mask = (bmap_obs['maskbmap']==1).flatten()
#bx,by = np.meshgrid(bmap_obs['xbmap'],bmap_obs['ybmap'], sparse=False, indexing='xy')
#x2 = np.array([bx,by]).T.dot(rot.T)
#rrx =  np.sqrt( (x2[:,:,0]/ratio)**2 + (x2[:,:,1])**2 )
##rrx = np.sqrt(bx**2+by**2).T
##rr = sqrt(xx[:,:,0]**2+yy[:,:,0]**2)
##figure()
##plot(np.log10(rrx.flatten()),bmap_obs['imhbmap'].flatten(),',')
#bmap_dat = bmap_obs['imhbmap'].flatten()[mask]
#bmap_mod = (emt*bmap_obs['scale'] + bmap_obs['bkg']).flatten()[mask]
#sigx = (1. + np.sqrt( bmap_obs['imhbmap'].flatten() + 0.75 ))[mask]
##chix = ( ( bmap_mod - bmap_dat) )
#Mbmap = bmap_obs['k']*(bmap_obs['scalebmap']*units.kpc.to("cm"))**2 *dz.to('cm').value
##bins = np.linspace(np.log10(10.),np.log10(sblim),20)
#bins = np.logspace( np.log10( np.min( np.abs( bmap_obs['xbmap'] ) ) ),
#                    np.log10( bmap_obs['xbmap'].max()), 20 )

##bprof =scipy.stats.binned_statistic(np.log10(rrx.flatten()),bmap_dat,bins=bins,statistic='mean')
##bfit = scipy.stats.binned_statistic(np.log10(rrx.flatten()),bmap_mod,bins=bins,statistic='mean')

#bprof =scipy.stats.binned_statistic( rrx.flatten()[mask],bmap_dat/sigx,bins=bins,statistic='sum')
#bfit = scipy.stats.binned_statistic( rrx.flatten()[mask],(bmap_mod)/sigx,bins=bins,statistic='sum')
#bprof2 = scipy.stats.binned_statistic( rrx.flatten()[mask], bmap_dat, bins=bins, statistic='mean' )

##bprof2 =scipy.stats.binned_statistic(np.log10(rrx.flatten()),bmap_dat,bins=bins,statistic='median')
##bfit2 = scipy.stats.binned_statistic(np.log10(rrx.flatten()),bmap_mod,bins=bins,statistic='median')

#bsig =scipy.stats.binned_statistic( rrx.flatten()[mask],sigx,bins=bins,statistic='mean')
#weigth =scipy.stats.binned_statistic( rrx.flatten()[mask],1./sigx,bins=bins,statistic='sum')
#Nbin = np.histogram( rrx.flatten(), bins=bins)


##figSB4, axSB4 = plt.subplots()

##kpc2amin = cosmo.arcsec_per_kpc_proper( redsh ).value / 60.
##bins1d = np.linspace( np.log10(0.25), np.log10(5.), 30 )

##rin = sb_obs['rin'] 
##rout = sb_obs['rout']
##sb_fit = np.zeros( rin.shape )
##asb = 1/3e4 / ( np.pi / ratio * ( rout**2 - rin**2 ) * kpc2amin**2 )


##for i in range(len(rin)):
##    ii2 = (pol_coord < rout[i]) & (pol_coord >= rin[i])
##    sb_fit[i] = ( emt[ii2] * bmap_obs['scale'] + bmap_obs['bkg'] ).sum() * asb[i]



##pippo = scipy.stats.binned_statistic(
##        np.log10( rrx.flatten() * kpc2amin ),
##        (emt*bmap_obs['scale'] + bmap_obs['bkg']).flatten(), 
##        bins=bins1d,
##        statistic='sum',
##        )
##rbin = ( pippo[1][:-1] + np.diff(pippo[1])/2. )# * kpc2amin
##asb = 1/3e4 / ( np.pi / ratio * np.diff( (10**pippo[1] )**2  ) )
##lineSB4, = axSB4.plot( sb_obs['r'], sb_fit)
##axSB4.errorbar( sb_obs['r'],  sb_obs['sb'], yerr=sb_obs['esb'], label=p)

##axSB4.loglog()
###axSB4.set_ylim( 1e-4, 1e1 )
##figSB4.show()


#figSB, axSB = plt.subplots()

##axSB.plot(10**(bprof[1][:-1]+np.diff(bprof[1])/2),(bprof[0]),'rs-')
#axSB.errorbar( bprof[1][:-1]+np.diff(bprof[1])/2, bprof2[0], yerr=bsig[0]/np.sqrt(Nbin[0]),fmt='rs-')

#lineSB, = axSB.plot( bfit[1][:-1]+np.diff(bfit[1])/2, (bfit[0]/weigth[0]),'b-')

##axSB.plot(10**(bprof[1][:-1]+np.diff(bprof[1])/2),(bprof2[0]/bchi[0]),'ms-')
##axSB.plot(10**(bfit[1][:-1]+np.diff(bfit[1])/2),(bfit2[0]/bchi[0]),'c-')

##axSB.plot(10**(bprof[1][:-1]+np.diff(bprof[1])/2),(bprof2[0]),'ms-')
##axSB.plot(10**(bfit[1][:-1]+np.diff(bfit[1])/2),(bfit2[0]),'c-')


#axSB.plot(rrx.flatten(),(bmap_obs['imhbmap'].flatten()),'k,')

#textSB = axSB.text(0.85,0.9,"%.2f"%(chi2[1]),transform=axSB.transAxes)
#axSB.semilogy()

##twxSB = axSB.twinx()
##bchi =scipy.stats.binned_statistic(np.log10(rrx.flatten()),chix,bins=bins,statistic='mean')

##lineSB2, = twxSB.plot(10**(bchi[1][:-1]+np.diff(bchi[1])/2),bsig[0],'gs-')
##lineSB3, = twxSB.plot(10**(bfit[1][:-1]+np.diff(bfit[1])/2),-((bfit[0]-bprof[0])/(1+np.sqrt(bprof[0]+0.75)))**2,'co-')

#axSB.set_xlim(0,sblim)
##axSB.set_ylim(0,1.)

#figSB.show()

##reb = 50
##bmap_modbin = np.reshape(emt*Mbmap+ bmap_obs['bkg'],(reb,emt.shape[0]//reb,reb,-1)).mean(-1).mean(1)
##bmap_obsbin = np.reshape(bmap_obs['imhbmap'],(reb,emt.shape[0]//reb,reb,-1)).mean(-1).mean(1)


##figSB2, axSB2 = plt.subplots()
##pippo = axSB2.imshow( np.log10( (emt*bmap_obs['scale'] + bmap_obs['bkg'] ) ), origin='lower',interpolation='none',)#vmax=2.6)
###pippo = axSB2.imshow(np.log10(emt*bmap_obs['scale']+ bmap_obs['bkg']),origin='lower',interpolation='none',vmin=0.,cmap=cmaps.viridis)
##figSB2.colorbar(pippo)
##figSB2.show()

##figSB3, axSB3 = plt.subplots()
##pippo = axSB3.imshow(bmap_modbin/bmap_obsbin -1., origin='lower', interpolation='none', vmin=-1., vmax=1., cmap=cmaps.viridis)
##figSB3.colorbar(pippo)




#fig, ax = plt.subplots()
#fig.subplots_adjust(bottom=0.2, left=0.1)
#xcut = Xtemp_obs['r']>0.
#line, = ax.plot(Xtemp_obs['r'],tfit_x0)
#ax.errorbar(Xtemp_obs['r'], Xtemp_obs['kt'], yerr=Xtemp_obs['ekt'], fmt='r-')
##ax.errorbar(Xtemp_obs2['rref_x'], Xtemp_obs2['kt'], yerr=Xtemp_obs2['ekt'], fmt='gd')
#ax.set_ylim(1,12)
#fig.show()



#	
#	

#def on_change(val):
#    for sldr in sliders:
#    #			print(sldr.label.get_text(),tpar[sldr.label.get_text()]['Observed'],sldr.val)

#        tpar[sldr.label.get_text()]['Value'] = sldr.val
#        if tpar[sldr.label.get_text()]['Observed'] >=0:
#            pos0[tpar[sldr.label.get_text()]['Observed']] = sldr.val
#		

##    args = ['SBT',tpar,xx,yy,zz,sl_obs,bmap_obs,Xtemp_obs,slscale,pscale,tscale,ratio,pa,True]
##    lnlike, (chi2 ,(dm2d, emt, tfit_x0)) = utils.lnprob(pos0,*args)
#    args = ( model.loglike, 'SBt', tpar,
#             coord, pol_coord,
#             sl_obs, bmap_obs, sb_obs, Xtemp_obs,
#             slscale, pscale, tscale,
#             ratio, pa, True,
#             spec, abring )
#    lnlike, (chi2 ,(dm2d, emt, sb_fit, tfit_x0)) = mcclusters.lnprob(pos0,*args)

#    #	axSL2.imshow(dm2d*slscale,origin='lower',interpolation='none',\
#    #	               extent=[sl_obs['x'][0],sl_obs['x'][-1],sl_obs['x'][0],sl_obs['x'][-1]])
##    axSL3.imshow( ( dm2d * slscale - sl_obs['avg'].reshape(dm2d.shape) )**2 \
##                    / sl_obs['fisher'].reshape(dm2d.shape)**2,
##                        origin='lower',interpolation='none',
##                       extent=[sl_obs['x'][0],sl_obs['x'][-1],sl_obs['x'][0],sl_obs['x'][-1]],
##    #                   vmin=-0.5,vmax=0.5,\
##                       cmap=cmaps.viridis)
#    #	#    imshow(suto.dm2d.value*slscale.value,origin='lower',interpolation='none',\
#    #	#       vmin=0,vmax=0.65,\
#    #	#)
#    #	sfit = scipy.stats.binned_statistic(rrs.flatten(),dm2d.flatten()*slscale,bins=20,range=[(10.,700)])
#    sfit = scipy.stats.binned_statistic(
#        rrs.flatten()[sl_obs['mask']],
#        dm2d.flatten()[sl_obs['mask']]*slscale , 
#        bins=sbins
#        )
#    lineSL.set_ydata(sfit[0])
#    textSL.set_text("%.2f"%(chi2[0]))

#    bmap_mod = (emt*bmap_obs['scale'] + bmap_obs['bkg']).flatten()[mask]
#    chix = ( (bmap_mod - bmap_dat)/(1. + np.sqrt( bmap_obs['imhbmap'].flatten()[mask] + 0.75 )) )**2
#    bfit = scipy.stats.binned_statistic( rrx.flatten()[mask],bmap_mod,bins=bins,statistic='mean')
#    bchi = scipy.stats.binned_statistic( rrx.flatten()[mask],chix,bins=bins,statistic='sum')

#    #	lineSB2.set_ydata(-bchi[0])
#    #	lineSB3.set_ydata(-((bfit-bprof[0])/(1+np.sqrt(bprof[0]+0.75)))**2)

#    lineSB.set_ydata((bfit[0]))
#    textSB.set_text("%.2f"%(chi2[1]))

##    pippo = scipy.stats.binned_statistic(
##            np.log10( rrx.flatten() * kpc2amin ),
##            (emt*bmap_obs['scale'] + bmap_obs['bkg']).flatten(), 
##            bins=bins1d,
##            statistic='sum',
##            )
##    rbin = ( pippo[1][:-1] + np.diff(pippo[1])/2. )# * kpc2amin
##    asb = 1/3e4 / ( np.pi / ratio * np.diff( (10**pippo[1] )**2  ) )
##    lineSB4.set_ydata( sb_fit )






##    axSB2.imshow( np.log10( (emt*bmap_obs['scale'] + bmap_obs['bkg']) ), origin='lower',interpolation='none',)#vmax=2.6)
#    #	axSB2.imshow(np.log10(emt),origin='lower',interpolation='none',\
#    #	               extent=[bmap_obs['xbmap'][0],bmap_obs['xbmap'][-1],bmap_obs['ybmap'][0],bmap_obs['ybmap'][-1]])
##    axSB2.imshow(np.log10(10**-5+emt*Mbmap+ bmap_obs['bkg']),origin='lower',interpolation='none',vmin=0.,\
##                   extent=[bmap_obs['xbmap'][0],bmap_obs['xbmap'][-1],bmap_obs['ybmap'][0],bmap_obs['ybmap'][-1]])

#    line.set_ydata(tfit_x0)

#    fig.show()
#    figSL.show()
##    figSL3.show()
##    figSB2.show()
#    figSB.show()
##    figSB4.show()


