from . import *

print('Initialising observables')


from abc import ABCMeta, abstractmethod
import numpy as np
import numpy as np
import scipy.integrate
import scipy.interpolate
import scipy.stats
from astropy import constants as cn
from astropy import units


class observable( object, metaclass=ABCMeta ):

#    __slots__ = [ 'obs', 'model', 'dimensions', 'mas' ]
    def __new__(cls, obs, coord, model, mask=None, dimensions=2, hyper=1., *args, **kwargs ):
        '''Factory method for base/subtype creation. Simply creates an
        (new-style class) object instance and sets a base property. '''
        instance = object.__new__(cls)

#        print("I'm in observable")
        instance.obs = obs
        instance.model = model
        instance.dimensions = dimensions
        instance.mask = mask
        instance.coord = coord
        instance.hyper = hyper
        
        return instance

    def __getnewargs__( self ):
        return self.obs, self.coord, self.model

    @abstractmethod
    def loglike( self ): pass

    def _rescale( self, mod  ):
        spl= scipy.interpolate.RectBivariateSpline( self.model.coord[0][:,0,0],
                                                    self.model.coord[1][0,:,0],
                                                    np.log10( mod ) )
        return 10**spl( *self.coord )

    def _profile( self, mod ):
        prof = scipy.stats.binned_statistic( self.pol_coord,
                                             mod,
                                             bins=self.bins, statistic='mean', )[0]
        return prof

class xray_sb( observable ):

    def __init__( self, scale, bkg, *args, **kwargs ):
        self.bkg = bkg
        self.scale = scale
        self.norm = -2 * np.sum( self.obs * np.log( self.obs ) - self.obs )
    
    def loglike( self ):
#        mod = ( np.maximum( 1e-25, self._rescale( self.model.em2d() ) ) \
#            * self.scale + self.bkg )[self.mask]
#        return -2 * np.sum( mod - self.obs + self.obs * ( np.log( self.obs ) - np.log(mod) ) )

        mod = ( self._rescale( self.model.em2d() ) * self.scale + self.bkg )[self.mask]
#        return -2 * np.sum( mod - self.obs * np.log( mod ) )
        return -2 * np.sum( mod - self.obs * np.log( mod ) ) + self.norm
#        mod = ( self._rescale( self.model.em2d() ) * self.scale + self.bkg )[self.mask]
#        return np.sum( scipy.stats.poisson( mod ).logpmf( self.obs ) )


class xray_sb_1d( observable ):

    def __init__( self, pol_coord, bins, scale, bkg, sigma, *args, **kwargs ):
        self.bkg = bkg
        self.scale = scale
        self.sigma = sigma
        self.pol_coord = pol_coord
        self.bins=bins

        
    
    def loglike( self ):

        mod = ( self._rescale( self.model.em2d() ) * self.scale + self.bkg )[self.mask]
        logl = -( ( self._profile(mod) - self.obs ) / self.sigma )**2
        return np.sum( logl[ ~np.isnan(logl) ] )
#        return np.sum( scipy.stats.norm.logpdf( mod, self.obs, self.sigma ) )



class xray_t( observable ):
    
    def __init__( self, pol_coord, sigma, *args, **kwargs ):
        self.pol_coord = pol_coord
        self.sigma = sigma

    def _profile( self, ):
        t2d = self.model.t2d()
        em2d = self.model.em2d()

        rin = self.coord[0] #Xtemp['rin'] 
        rout = self.coord[1] #Xtemp['rout']
        redges = np.append( self.coord[0], self.coord[1][-1] )

        ii2 = np.digitize( self.pol_coord, bins=redges )
        weigth = np.maximum( 1e-20, np.bincount( ii2.flat, em2d.flat ) )
        tfit_x0 = ( np.bincount( ii2.flat, (t2d * em2d).flat ) / weigth )[1:-1]

        return tfit_x0

    
    def loglike( self ):
        mod = self._profile()
#        logl = scipy.stats.norm.logpdf( mod, self.obs, self.sigma )
        logl = -( ( mod - self.obs ) / self.sigma )**2
        return np.sum( logl[ ~np.isnan(logl) ] )


class lensing_fisher( observable ):
    
    def __init__( self, scale, fisher, *args, **kwargs ):
        self.fisher = fisher
        self.scale = scale
    
    def loglike( self ):
        mod = np.maximum( 0., self._rescale( self.model.dm2d() ) )[self.mask]
        dsigma = mod * self.scale - self.obs
        return -np.dot( dsigma, np.dot( self.fisher, dsigma ) )


class lensing( observable ):
    
    def __init__( self, scale, sigma, *args, **kwargs ):
        self.sigma = sigma
        self.scale = scale
    
    def loglike( self ):
        mod = np.maximum( 0., self._rescale( self.model.dm2d() ) )[self.mask]
        logl = -( ( mod*self.scale - self.obs ) / self.sigma )**2
        return np.sum( logl )
#        return np.sum( scipy.stats.norm.logpdf( mod*self.scale, self.obs, self.sigma ) )

class zeldovich( observable ):
    
    def __init__( self, scale, sigma, *args, **kwargs ):
        self.sigma = sigma
        self.scale = scale
    
    def loglike( self ):
        mod = np.maximum( 0., self._rescale( self.model.dt2d() ) )[self.mask]
        return np.sum( scipy.stats.norm.logpdf( mod*self.scale, self.obs, self.sigma ) )

