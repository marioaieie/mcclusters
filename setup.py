#!/usr/bin/env python

from setuptools import setup

setup(
   name='mcclusters',
   version='0.9.0',
   author='Mario Bonamigo',
   author_email='mario.bonamigo@lam.fr',
   packages=['mcclusters','mcclusters.models'],
   )
